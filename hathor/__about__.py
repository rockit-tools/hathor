from hathor import __version__

author_name = "BrinkerVII"
author_email = "brinkervii@gmail.com"
package_version = __version__
package_description = "A lua script combobulator aimed at roblox"
package_license = "GPL-3"
package_repository = "https://gitlab.com/rockit-tools/hathor"
