local items = {
    {
        ID = 0,
        Name = "Hammer"
    },
    {
        ID = 1,
        Name = "Screwdriver"
    },
    {
        ID = 2,
        Name = "Saw"
    },
    {
        ID = 3,
        Name = "Duct Tape"
    },
    {
        ID = 4,
        Name = "Butter Knife"
    }
}

return items
